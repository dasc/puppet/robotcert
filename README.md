# robotcert

#### Table of Contents

1. [Description](#description)
1. [Setup requirements](#setup-requirements)
1. [Usage - Configuration options and additional functionality](#usage)

## Description

This module will help keep a robot proxy certificate up to date, given a robot
keytab.

Two services are used.  The first runs 'kinit' on the keytab to generate a kerberos ticket.  The second runs 'ligo-proxy-init' to generate a proxy certificate good for 11 days.

## Setup

### Setup Requirements

Management of the origin keytab is not handled by this module.  The user is required to install the keytab in some other manner.

## Usage

To manage a robot proxy ceritificate in /etc/grid-security/proxies/pulsar.pem, using the existing keytab file in /etc/grid-security/pulsar.keytab, and updating the proxy weekly, use the following code:

```
class {'::robotcert':
    keytabs => {
	'pulsar' => {
	    keytab_file => '/etc/grid-security/pulsar.keytab',
	    proxy_file => '/etc/grid-security/proxies/pulsar.pem',
	    principal => 'pulsar/robot/ldas-pcdev1.ligo-la.caltech.edu@LIGO.ORG',
	},
    },
}
```

To create a proxy owned by a particular user or group, use the 'user' and/or 'group' settings:

```
class {'::robotcert':
    keytabs => {
	'pulsar' => {
	    keytab_file => '/etc/grid-security/pulsar.keytab',
	    proxy_file => '/etc/grid-security/proxies/pulsar.pem',
	    principal => 'pulsar/robot/ldas-pcdev1.ligo-la.caltech.edu@LIGO.ORG',
	    interval => '1w',
	    user => 'pulsar',
	    group => 'pulsar',
	},
    },
}
```

Or you can define the settings using hiera.  In an appropriate yaml file, use the following:

```
robotcert::keytabs:
  pulsar:
    user: 'pulsar'
    group: 'pulsar'
    interval: '1w'
    keytab_file: '/etc/grid-security/pulsar.keytab'
    proxy_file: '/etc/grid-security/proxies/pulsar.pem'
    principal: 'pulsar/robot/ldas-pcdev1.ligo-la.caltech.edu@LIGO.ORG'
```

Then use the following puppet code to instantiate the class and let hiera find the keytab and cert information:

```
class {'::robotcert': }
```
