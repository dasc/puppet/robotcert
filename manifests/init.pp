class robotcert (
    Robotcert::Certspec $keytabs,
) {
    file {'/usr/lib/systemd/system/kerberos-ticket@.service':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet://puppet/modules/robotcert/usr/lib/systemd/system/kerberos-ticket@.service',
        notify => Exec['systemd-reload-robotcert'],
    }

    file {'/usr/lib/systemd/system/kerberos-ticket@.timer':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet://puppet/modules/robotcert/usr/lib/systemd/system/kerberos-ticket@.timer',
        notify => Exec['systemd-reload-robotcert'],
    }

    file {'/usr/lib/systemd/system/robot-certificate@.service':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet://puppet/modules/robotcert/usr/lib/systemd/system/robot-certificate@.service',
        notify => Exec['systemd-reload-robotcert'],
    }

    file {'/usr/lib/systemd/system/robot-certificate@.timer':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet://puppet/modules/robotcert/usr/lib/systemd/system/robot-certificate@.timer',
        notify => Exec['systemd-reload-robotcert'],
    }

    file {'/usr/lib/systemd/system/robot-scitoken@.service':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet://puppet/modules/robotcert/usr/lib/systemd/system/robot-scitoken@.service',
        notify => Exec['systemd-reload-robotcert'],
    }

    file {'/usr/lib/systemd/system/robot-scitoken@.timer':
        ensure => 'file',
        owner  => 'root',
        group  => 'root',
        mode   => '0644',
        source => 'puppet://puppet/modules/robotcert/usr/lib/systemd/system/robot-scitoken@.timer',
        notify => Exec['systemd-reload-robotcert'],
    }

    exec {'systemd-reload-robotcert':
        command => '/bin/systemctl daemon-reload',
        refreshonly => true,
    } -> Service<||>

    $robotcert::keytabs.each |String $name, Hash $service_hash| {
        robotcert::keytab {"${name}":
            * => $service_hash,
        }
    }
}
