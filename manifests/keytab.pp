define robotcert::keytab (
    Boolean $service_enable = true,
    Enum['running', 'stopped'] $service_ensure = 'running',
    String $principal,
    String $keytab_file,
    Optional[String] $proxy_file = undef,
    String $user,
    String $group,
    String $interval,
    Optional[Struct[{vault => String,
                     issuer => String,
                     audience => String,
                     role => String,
                     scopes => String,
                     credkey => String,
                     token_file => String,}]] $token_settings = undef,
    Optional[String] $keytab_base64 = undef,
    Optional[Hash[String, String]] $extra_vars = {}
) {
    include ::robotcert

    if $keytab_base64 {
        file {"${keytab_file}":
            ensure => 'file',
            owner => $user,
            group => $group,
            mode => '0400',
            content => base64('decode', $keytab_base64),
        }
    }

    file {"/etc/sysconfig/kerberos-ticket-${title}":
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => epp('robotcert/sysconfig.epp', {
            principal   => $principal,
            keytab_file => $keytab_file,
            proxy_file  => $proxy_file,
            token_settings  => $token_settings,
            extra_vars  => $extra_vars,
        }),

        notify => Service["kerberos-ticket@${title}.service"],
    }

    service {"kerberos-ticket@${title}.service":
        enable  => $service_enable,
        #ensure  => $service_ensure,
    }

    service {"kerberos-ticket@${title}.timer":
        enable  => $service_enable,
        ensure  => $service_ensure,
    }

    file {"/etc/systemd/system/kerberos-ticket@${title}.service.d":
        ensure => 'directory',
        owner  => 'root',
        group  => 'root',
        mode   => '0755',
    }

    file {"/etc/systemd/system/kerberos-ticket@${title}.service.d/user.conf":
        ensure  => 'file',
        owner   => 'root',
        group   => 'root',
        mode    => '0644',
        content => epp('robotcert/user.conf.epp', {
            user   => $user,
            group  => $group,
        }),
        notify  => [Exec['systemd-reload-robotcert'], Service["kerberos-ticket@${title}.service"]],
    }

    if $proxy_file {
        service {"robot-certificate@${title}.service":
            enable  => $service_enable,
            #ensure  => $service_ensure,
        }

        service {"robot-certificate@${title}.timer":
            enable  => $service_enable,
            ensure  => $service_ensure,
        }

        file {"/etc/systemd/system/robot-certificate@${title}.service.d":
            ensure => 'directory',
            owner  => 'root',
            group  => 'root',
            mode   => '0755',
        }

        file {"/etc/systemd/system/robot-certificate@${title}.service.d/user.conf":
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => epp('robotcert/user.conf.epp', {
                user   => $user,
                group  => $group,
            }),
            notify  => [Exec['systemd-reload-robotcert'], Service["robot-certificate@${title}.service"]],
        }
    }

    if $token_settings {
        service {"robot-scitoken@${title}.service":
            enable  => $service_enable,
            #ensure  => $service_ensure,
        }

        service {"robot-scitoken@${title}.timer":
            enable  => $service_enable,
            ensure  => $service_ensure,
        }

        file {"/etc/systemd/system/robot-scitoken@${title}.service.d":
            ensure => 'directory',
            owner  => 'root',
            group  => 'root',
            mode   => '0755',
        }

        file {"/etc/systemd/system/robot-scitoken@${title}.service.d/user.conf":
            ensure  => 'file',
            owner   => 'root',
            group   => 'root',
            mode    => '0644',
            content => epp('robotcert/user.conf.epp', {
                user   => $user,
                group  => $group,
            }),
            notify  => [Exec['systemd-reload-robotcert'], Service["robot-scitoken@${title}.service"]],
        }
    }
}
